package ru.t1.sochilenkov.tm.api.repository.dto;

import ru.t1.sochilenkov.tm.dto.model.ProjectDTO;

public interface IProjectDtoRepository extends IUserOwnedDtoRepository<ProjectDTO> {
}
